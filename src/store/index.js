import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    activeIndex: 0,
    activeData: {},
    pageType: '',
    fields: []
  },
  mutations: {
    SET_ACTIVE_INDEX(state, data){
      state.activeIndex = data
    },
    SET_ACTIVE_DATA(state, data){
      state.activeData = data
    },
    SET_PAGE_TYPE(state, data){
      state.pageType = data
    },
    SET_FIELDS (state, data) {
      state.fields = data
    }
  }
  
})
