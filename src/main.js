import './public-path'
import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import store from './store'
import VueRouter from 'vue-router'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.config.productionTip = false
let instance = null
function render (props = {}) {
  const { container } = props
  instance = new Vue({
    store, 
    data () {
      return {
        my: 'title'
      }
    },
    render: h => h(App)
  }).$mount(container ? container.querySelector('#app') : '#app')
}

if (!window.__POWERED_BY_QIANKUN__) {
  render()
}

export async function bootstrap () {
  console.log('[vue] vue app bootstraped')
}

export async function mount (props) {
  //接收主应用传递过来的props 挂载到原型上让整个子应用vue实例都可以使用
  Vue.prototype.$masterProps = props
  // console.log('[vue] props from main framework', props)
  render(props)

  //子应用通过主工程传递过来的事件属性做监听通信
  props.onGlobalStateChange((state, prev) => {
    console.log(state, prev, '%c********props.onGlobalStateChange********', 'color:red')
  })
}

export async function unmount () {
  instance.$destroy()
  instance = null
}
