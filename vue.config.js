const path = require('path');

module.exports = {
  devServer: {
    open: true
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, 'src/'),
        '@styles': path.resolve(__dirname, 'src/styles'),
        '@library': path.resolve(__dirname, 'src/library'),
        '@public': path.resolve(__dirname, 'public/')
      }
    }
  },
  publicPath : './',
  productionSourceMap: false
};
